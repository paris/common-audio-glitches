# Common audio glitches

Here are a bunch of common things that can go wrong when you process 
audio. Try them with different sounds to see how they behave. If you 
can't tell the problem by listening, keep trying (also use decent 
speakers/headphones). You need to develop the ability to catch such 
problems. This also means that when you develop audio algorithms you 
need to get into the habit of listening at the output to help you with 
debugging. Some of these problems are unavoidable, some are easily 
fixed. But in all cases you should know which part of the code produces 
such issues.

-- Paris
